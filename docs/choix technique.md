# Choix technique

!!! attention 
    Le pipeline de traitement de donnée est construit en fonction des différentes recommandations et spécifications validées pendant le projet. Si des modifications sont effectuées sur un élément de la chaîne de traitement (exemple modification d'un type de données, d'un champs de fichier CSV), cela induira des erreurs dans les résultats.
    Il est recommandé de valider tout changement avant modification.

## livrables

![schéma](livrabletic.png)

## Pipeline ETL

![Schéma pipeline ETL](pipelinetic.png)  

## Serie temporelle  

Le projet traite de données météo et de série temporelle
Les séries temporelles nous aident à identifier des tendances dans les données, pour montrer concrètement ce qui est arrivé dans le passé et formuler des estimations.

### PostgreSQL

Le SGBDR PostgreSQL est plus adapté a la gestion des série temporelle et implémente :
Types temporels : timestamp
timestamp (without time zone !)
représente une date et une heure
fuseau horaire non précisé
Utilisation :
stockage d’une date et d’une heure  

``` sql
SELECT now()::timestamp ;

            now             
----------------------------
 2019-11-13 15:20:54.222233  
```

### MCD

![MCD BDD ETIC](mcdatabasetic.png)

### Données Brutes

les données sont fournies par ETIC (Emmanuel) pour le site de Lyon ou Odile pour le site de Grenoble. 

!!! attention
    les formats de fichier transmit ne sont pas au UTF-8 mais au format ISO-8859-1 (latin-1)




### Convention de nommage 

Le nom d’un fichier doit être succinct et précis. Il ne doit pas dépasser 31 caractères maximum, extension comprise.
Sur la base des noms existants, proosition d'une convention de nommage à valider avec le client.

![Batiments ETIC](batimentetic.png)

Les caractères à proscrire:

les quatre signes diacritiques suscrits sur les voyelles (é à ï ), pas de cédille (ç). 

!!! danger
    Ne pas utiliser de caractères spéciaux (sauf underscore « _ » )
    % $ ! & / \ : ; « » % & # @ etc.  
    Ne pas utiliser de mots vides : le, la, les, un, une, des, et, ou, etc.  
    Ne pas utiliser de dénominations vagues (exemple : « divers », « autres », « à classer », etc.).  

!!! success "Proposition de nomenclature"
    
    LYO__101__AAAA-MM-JJ__serial(9).csv   le nom du fichier + extention sur 31 caractères  
    GRE__201__AAAA-MM-JJ__serial(9).csv


### Traitement Pipeline

[Taille réel schéma Traitement Pipeline](https://excalidraw.com/#json=6425151356796928,dhwoYDUfwf0vaZu3Gz6p3g)

![Traitement Pipeline](traitementETL.png)