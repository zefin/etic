  **<center>Analyse des températures des Espaces ETIC.</center>**  

![Thermometre](/thermometre.jpg)


Les dashboards générés grâce à vos collectes de données de températures permettent d'analyser facilement les variations de température par espace via la visualisation des données historiques. Le projet fournira à terme une solution peu coûteuse pour évaluer l'efficacité de vos installations. 


-----
### Caractéristiques

- [X] Visualisation des données historiques de température par espace 
- [X] Evolution de la température heure/heure.
- [X] Mise en évidence de la température max et min, le jour et et la nuit sur la période observée.
- [X] Calcul du taux d’inconfort : correspond au nombre d’heures au-dessus de 28, 29, 30, 31, sur la période observée
- [X] En zoomant sur un jour en particulier, correspond à la part des heures au-dessus de 28, 29, 30, 31
- [X] Comparez les données globales aux conditions météorologiques locales à l'aide d'une API météo locale.
- [X] Script python API OpenWheatherMap. 

Le projet minimal consiste en un déploiement de Python / Jupyter NoteBook.


### API OpenWeatherMap

Nous avons choisi d'utiliser le service API REST [OpenWeather](https://openweathermap.org/history) pour publier et récupérer des données.  
Une clé API est nécessaire pour envoyer des requêtes GET et POST à un canal privé.  
La documentation peut être trouvée [ici] (https://openweathermap.org/price) "Get API key"  

Les scripts Python doivent être exécutés tous les jours a 00:00 en ajoutant le travail suivant à crontab ou Gitlab CI si linux, sinon utiliser le planififcateur de tâches Microsoft 

Exemple de configuration avec crontab sous Linux
```
0 0 * * * cd "~/git/dashboard-temperatures-etic/code" && /path/to/pipenv run python3 /main_COOP.py
0 0 * * * cd "~/git/dashboard-temperatures-etic/code" && /path/to/pipenv run python3 /main_HEVEA.py
```

Où `pipenv` est un chemins complet. 


### Repository GITLAB

Le paramétrage GITLAB concernant La visibilité du repository est positionnée sur PRIVEE avec comme invité:  

- equipe@turbine.coop   
- emmanuel.courbois@etic.co  



### Installation

Creer un dossier racine pour cloner le dépot Gitlab

```
$ mkdir 'nom_repertoire'   # création repertoire
$ cd 'nom_repertoire'      # se déplacer dans le repertoire
```

Cloner le repository Gitlab  

```
$ git clone https://gitlab.com/zefin/dashboard-temperatures-etic.git   # deploiement du dépot Gitlab dans `nom_repertoire`
```  

Le projet utilise [Pipenv](https://github.com/pypa/pipenv) pour gérer les bibliothèques Python suivantes dans un virtualenv:  
[requires]
python_version = "3.8"  

```
$ pipenv install jupyter ipwidgets pandas matplotlib plotly seaborn voila   # installation des librairies Python utilisées pour le projet
$ pipenv shell                                                              # lancement de l'environnement virtuel 
$ python3 main_COOP.py                                                      # lancement API OpenWeather Grenoble
$ python3 main_HEVEA.py                                                     # lancement API OpenWeather Lyon
$ jupyter notebook                                                          # lancement de Jupyter Notebook
```

### Documentation

Lorsque que le repository est cloné, on doit retrouver l'arborescence suivante:

![Dashboard](/dashboard_etic_1.png)  

Les scripts Python :  

- main_COOP.py
- main_HEVEA.py

```
  - récupèrent les données du jour de l'API OpenWeatherMap,   
  - les insèrent dans les fichiers CSV temp-ext_COOP.csv et temp_ext_HEVEA.csv  dans le répertoire `"~/dashboard_etic/temp_ext/" `   
  - copie le template Jupyter Notebook `API_Openweather.ipynb` dans le dossier de destination `"~/dashboard_etic/temp_ext/" `  
  - sauvegarde les 2 CSV avant modification dans `"~/dashboard_etic/bck_temp_ext"`  
  - crées l'arborescence ci-dessous
```
![main](/main_data.png)
![temp](/data_temp.png)

Le repertoire `dashboard_etic` est le dossier racine de l'installation, il contient les repertoires `bck_temp_ext` et `temp_ext`.  
le repertoire `temp_ext` contient le Notebook jupyter qui permet de visualiser les températures extérieur de Grenoble et de Lyon (voir paragraphe Installation lancement Jupyter Notebook)  

Les Notebook contenant les données de températures autres que les températures exterieurs sont dans le dossier Notebook_visualisation et sont accessibles de la façon suivante:

```
$ cd Notebook_visualisation
$ pipenv install jupyter ipwidgets pandas matplotlib plotly seaborn voila   # installation des librairies Python utilisées pour le projet
$ pipenv shell                                                              # lancement de l'environnement virtuel 
$ jupyter notebook                                                          # lancement de Jupyter Notebook
```
### lancement jupyter Notebook  

![notebook_1](/jupyter_notebook.png)

### Lancer le notebook   

![notebook_2](/notebook_1.png)

### cliquer sur Voilà

![voila](/voila.png.svg)

![voila_1](/voila_1.png)


### le Dashboard est prêt à être exploité  

![voila_2](/voila_2.png)

### le principe est le même pour tous les dashboard