## Présentation Widgets

[Widget Etic](https://colab.research.google.com/drive/1wUVhaCsBxQ_bBkR6nHFA3TSAdVktjLPe?usp=sharing)


## Graphiques Notebook

![COOP 2020](coop_2020.png)
![enercoop mezz](enercoop_mezz.png)
![Widget Etic](widget_etic.png)

## Graphiques Metabase

![Moyenne des T° 101 UCPA Juillet 2020](Etic2007101UCPA.png)
![Focus 2007 101 UCPA Juillet](FocusETIC2007101UCPA.png)
![Focus 2007 101 UCPA Juillet](ETIC2007101UCPA1.png)

## Graphique initial
![Graphique demo](graphique.png)
