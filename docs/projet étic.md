## Cahier des charges Projet étic

### Contexte

ETIC exploite **6 espaces de travail** (Paris, Lyon, Castres et Grenoble) et **4 autres sont en projet** (Lille/Paris/Toulouse/Marseille). 

Des tiers-lieux qui rassemblent plus de 150 structures engagées, réparties sur toute la France, qui partagent au quotidien des espaces de travail et de vie, pour créer ensemble des écosystèmes riches de synergies et de partages.


ETIC crée, finance et gère des espaces de bureaux et de commerces dédiés aux acteurs du changement sociétal et à haute qualité sociale et environnementale.
**Emmanuel Courbois**, qui est Responsable Hygiène Sécurité Environnement et Développement Durable chez ETIC, **pour pouvoir mesurer la performance des bâtiments**, **prélève les températures, étage par étage de chacun des bâtiments**, par le biais d'un **fichier .txt ou .csv** généré par un thermomètre enregistreur USB. Le thermomètre permet de restituer les températures à la fréquence que l’on souhaite.
A partir de ce fichier, il peut alors analyser la performance du bâtiment, notamment en comparant avec la température extérieure, et en repérant les situations alarmantes, comme les dépassements de température en cas de canicule par exemple.

### Enjeu

Cet exercice d'**extraction**, de **traitement** et d’**analyse des données** de température est aujourd’hui mené **manuellement**. La collecte des données continuera à se faire manuellement : le responsable du bâtiment, au moment souhaité, récupère le fichier source .csv des températures en connectant le
thermomètre en USB à son ordinateur.
**L'enjeu est donc de pouvoir automatiser la partie traitement et analyse des données** dans le but de gagner en efficacité, en visibilité et pour identifier de potentielles alertes.  
==Le **principal indicateur recherché est un taux d’inconfort** estimé au temps où la température est supérieure
à 28° (avec un palier à 29 et 30° au-delà de ce seuil) comparé au temps total annuel et au temps de la période observée.==

### Besoin

A partir des fichiers de données de températures relevés depuis les différents thermomètres(heure/heure):  

1. Mettre en place un FTP de dépôt (structure de dossier à prévoir par bâtiment/étage)
2. Générer un fichier de données cible dans un dossier sur ce même FTP après le traitement de données nécessaires avant exploitation (mise au format, agrégation des fichiers...)
3. Mettre en place un tableau de bord permettant de visualiser un certain nombre de métriques, selon les critères de filtres suivants :  

    - [x] par bâtiment / étage
    - [x] sur une période donnée (date/heure de début – date/heure de fin).  

Cet outil pourra être installé sur une machine en local ou déployé sur un serveur et accessible via une page de backOffice avec une sécurité d’accès (login/mot de passe).  

Les métriques à mettre en évidence selon la Datavisualisation de votre choix, sont les suivantes:   


- [x] Evolution de la température heure/heure.  
- [ ] Mise en évidence de la température max et min, le jour et et la nuit sur la période observée.  
- [ ] Mise en évidence de la température moyenne le jour et la nuit, calculée sur la période observée.  
- [ ] Calcul du taux d’inconfort : correspond au nombre d’heures au-dessus de 28, 29, 30, 31, sur la période observée et sur l’année. Par exemple, un taux d'inconfort de 15% sur la période estivale revient peut être à 2% de l'année : indicateur d'aide à la décision pour engager des investissements.  
- [x] En zoomant sur un jour en particulier, correspond à la part des heures au-dessus de 28, 29, 30, 31 :  
- [ ] Comparatif avec la température extérieure, heure/heure, qui sera récupérée et stockée en base, depuis l'API de OpenWeatherMap  

Données sources :
fichier .csv (un par thermomètre, sans reprise d’historique à chaque génération de fichier)
Données météo : https://openweathermap.org/api/one-call-api#history

### Livrable attendu

- Le ftp et les accès pour importer les fichiers csv, par thermomètre
- Un projet à installer en local sur une machine ou une page web déployée avec identification
sécurisée intégrant le tableau de bord et ses métriques
- Le projet accessible sur un Git, de manière à ce qu’ETIC puisse réutiliser le projet

Document décrivant :
- La démarche du traitement des données qui a été effectuée et la modélisation de la base de
données
Un document plus « projet » pourra également être remis
- Process d’analyse
- Les choix techniques et fonctionnels effectués pour répondre au besoin
