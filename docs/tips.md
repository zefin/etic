???+ tip
    https://pypi.org/project/PyDrive/ 

    PyDrive est une bibliothèque d'encapsuleurs de google-api-python-client qui simplifie de nombreuses tâches courantes de l'API Google Drive.     

???+ question
     Infrastructure existante chez le client ?  
     outils de gestion du projet ? Gitlab ou Trello  

     Localisation des batiments (adresses, étages...) 
     ETIC exploite 6 espaces de travail (Paris, Lyon, Castres et Grenoble) et 4 autres sont en projet (Lille/Paris/Toulouse/Marseille).

        HEVEA   2 Rue Professeur Zimmermann, 69007 Lyon  1er centre ETIC de Lyon  
        Place Gabriel Rambaud 69001 Lyon Auvergne-Rhône-Alpes, FR  
        31, Rue Gustave Eiffel 38000 Grenoble Auvergne-Rhône-Alpes, FR  
        22, Rue Mérigonde 81100 Castres Occitanie, FR  

     

     FTP ou solution alternative ? (voir ==sftp==)   
      
     Nombre de sonde de T° modèle  
     Horaire de jour  / Horaire de nuit = occupé / pas occupé ? Week-end jour fériés ?
     
     le besoin semble pouvoir s'articuler autour de 3 axes:

     - vues ponctuelles = DASHBOARD 
     - présentation sous forme de tableau dynamique 
     - Outils / Fonction de concaténation de fichiers 
        doit être evolutive si besoin d'ajouter des sites ??