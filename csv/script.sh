# Download dataset into data_SNCF.csv
echo "Téléchargement de données en cours..."
#wget -q --show-progress -O /home/arnaud/activites/Projet_1/data_SNCF.csv "https://ressources.data.sncf.com/explore/dataset/regularite-mensuelle-tgv-aqst/download/?format=csv&timezone=Europe/Berlin&lang=fr&use_labels_for_header=true&csv_separator=%3B"
echo "Téléchargement terminé. Transformation des données en cours..."

# Import dataset into database
sqlite3 etic.db < import.sql


echo "Script terminé"

