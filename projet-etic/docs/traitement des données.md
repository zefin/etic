# Données brutes

Chaque fois que nous mesurons un élément et que nous enregistrons une valeur temporelle avec sa mesure, nous construisons une série temporelle. Les séries temporelles nous entourent :
- consultation des prévisions météorologiques avec l'évolution des températures nous observons une série temporelle.
Les séries temporelles nous aident à identifier des tendances dans les données, pour montrer concrètement ce qui est arrivé dans le passé et formuler des estimations éclairées de ce qui va se produire dans le futur.




## API OpenWeathermap

1. Access to high-end servers
- URL to access to high-end servers for your account: 
history.openweathermap.org
- Example of API call:
http://history.openweathermap.org/data/2.5/history/city?id=2885679&type=hour&appid=053c09565de3a966efa5b8d39b2969d9

2. API key (APPID)
- Please, always use your API key ( 053c09565de3a966efa5b8d39b2969d9 ) in any API call. 
- We keep right to refuse API calls that do not content API key
- Read more about it http://openweathermap.org/appid#use

[OpenWeather Bulk](https://openweathermap.org/history-bulk)

[Données historique OpenWeather](http://history.openweathermap.org/data/2.5/history/city?q={city ID},{country code}&type=hour&start={start}&end={end}&appid={API key})

[Graphique demo](/graphique.png)


