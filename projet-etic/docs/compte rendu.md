## Documents partagés

[Point Etic - Turbine](https://docs.google.com/document/d/13U3fFmE5D7wIOP5MFb661kTzGFB5jnqV4Fkg0QZimfs/edit)

## 24 mars 2021 - Réunion Pré projet turbine


#### Interlocuteur  
1 Turbine   
1 réfèrent client    
1 formateur Simplon 

Point hebdo + possibilité échange dans la semaine   
Posture gestion projet rapport client = canal de communication avec le client   
Sprint hebdo    


ETIC: 
Emmanuel Courbois  
 [Linkdedin étic](https://fr.linkedin.com/company/etic-france) 
Activité Création de tiers lieux   

    - Performance environnemental / bureau   / pas de climatisation lorsque c'est possible   
    - Qualité environnementale    
    - Empreinte carbone   
    - Pbm de température  surtout l'été    
    - Comprendre la pbmtic client  
    - Campagne de mesure    
    - Thermomètre sous forme de clé usb => les données sont collectées   
    - TBD mesure du taux d'inconfort nombre d'heure > 28°   
    - Fichier CSV    
    - Besoin de stockage de données   
    - Comparaison avec des données extérieur   
    - Pas de publication public    


## 29 mars 2021 - Session de Débrief (Turbine). 30min

Présent Emmanuel (etic) et Odile (turbine)

campagne de T° de mai à septembre 
correlation des temperatures INT et EXT sur même graphique
Solution FTP ethique (ni Google ni Amazon ) solution OpenSource voir FireFox

Mise en evidence du taux d'inconfort sur l'année
Mise en évidence du taux d'inconfort sur la période observée (Mai à Septembre)

Prochain RDV le 09/04 15 heure

Préparer les 3 Sprints précisement

==Penser a recontextualiser au debut du point== 

## 09 avril 2021 Démarrage environnement technique

Présent Emmanuel (étic) Odile (Turbine) et Romain (Simplon)

pbm rencontrés:  hétérogénéité des données (nom de fichiers, date et heure de prise de température)  

demande de convention de nommage des salles et bat  
demande de modification encodage UTF-8 a la place de l'encodage ISO-8859-1 (latin-1)  
Modification bdd ajout de champs T° EXT et T° INT.  
==Rester focus sur l'objectif Mise en evidence du taux d'inconfort sur l'année Mise en évidence du taux d'inconfort== 

> Pour le sprint 1  

- livraison bdd 
- Graphique

## 15 avril 2021 Point Livrable 1

Présent Emmanuel (étic) Odile (Turbine) et Romain (Simplon)

Résumé du précédent SPRINT
Point sur avancement construction BDD => fonction d'encodage des fichiers OK
point sur les différents vsuels => le choix pourrait se porter sur l'outil de visualisation WIDGET NOTEBOOK
Démo réalisée avec Emmanuel

> Pour le Sprint 2  

- présenter un visuel avec corrélation températures exterieur 
- présenter un visuel avec des moyennes  
- Mise en évidence de la température max et min, le jour et et la nuit sur la période observée.

## 21 avril 2021 Point Livrable 2

Odile (Turbine)  Romain et Pierre Loîc (Simplon)

Résumé du précédent SPRINT
 
- présenter un visuel avec corrélation températures exterieur 

> Pour le Sprint 3

- présenter un visuel avec des moyennes sur le MOIS et sur la SEMAINE
- Mise en évidence de la température max et min, le jour et et la nuit (08h - 20h) sur la période observée.
- Travail sur l'API openwheather 

## 28 avril 2021 Point Livrable 3

Odile (Turbine) Emmanuel (ETIC) Romain et Pierre Loîc (Simplon)

Résumé du précédent SPRINT
 
- présentation des visuels avec des moyennes / MOIS et / SEMAINE
- Mise en évidence de la température max le jour 
- Travail sur l'API openwheather 

> Pour le sprint final

- solution FTP ou solution autre
- modifier visuel sur la série MAX pas de PIE 
- feu vert pour interpolation des données historiques pour API 
- présenter un visuel pour calcul de l'inconfort > 28°