#  Envoyer des flux via SFTP

!!! important
Le client ne souhaite pas utiliser d'application Google Drive ou autre applications qui ne correspondent pas à "l'ethique" client
La proposition d'utiliser une application Google pour le transfert de fichier n'est pas validée.

https://support.google.com/merchants/answer/160627?hl=fr

## Créer un compte SFTP
Pour créer un compte SFTP ou FTP, procédez comme suit :

Connectez-vous à votre compte Merchant Center.
Dans votre compte, accédez au menu déroulant représenté par une icône à trois points, puis cliquez sur SFTP/FTP/GCS.
Cliquez sur l'onglet SFTP/FTP.
Choisissez entre le protocole SFTP ou FTP (nous vous recommandons vivement d'utiliser le protocole SFTP). Si vous souhaitez utiliser le protocole FTP, développez la section "Créer un compte FTP".
Un nom d'utilisateur associé au compte SFTP ou FTP est généré automatiquement. Cliquez sur Générer le mot de passe pour obtenir un mot de passe et activer votre compte. 
Un mot de passe généré automatiquement s'affiche alors dans une fenêtre pop-up.


;x.)H0:{9_
:9d9!1S=87

https://support.google.com/merchants/answer/185963?hl=fr


sudo dnf install filezilla
ftp -P 19321 mc-sftp-370196609@partnerupload.google.com

