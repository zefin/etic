---
# Dashboard de suivi des températures   
---

<p align="center">
  <img width="150" height="150" src="logo-ETIC5.jpg">
</p>

#### Mise en page du projet

    mkdocs.yml    # Fichier de configuration.
    docs/
        index.md  # Page d'accueil de la documentation.
        ...       # Autres pages, images et fichiers.  
