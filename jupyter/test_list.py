import requests as re
import pandas as pd
from pandas.io.json import json_normalize
import json
import csv
import time
import datetime
from datetime import date
from datetime import datetime
import os
import os.path
import shutil

data_path = "~/dashboard_etic/temp_ext/"     # repertoire de collecte des températures extérieur
bck_path = "~/dashboard_etic/bck_temp_ext/"  # repertoire de backup des températures extérieur
full_path = os.path.expanduser(data_path)    # création des repertoires de base utilisateur ~ symbole tilde
full_path_bck = os.path.expanduser(bck_path) 
list_dir = [full_path, full_path_bck]
isfile = os.path.isfile(data_path)

d = datetime.now()
print(d)
unixtime = time.mktime(d.timetuple())
#print(unixtime)
fic_csv = "temp_ext_COOP.csv"
fic_json = "temp_ext_COOP.json"


def backup_data1():
    for i in os.listdir((full_path)):
        print(i, "est un fichier")

shutil.copyfile(full_path + fic_csv, full_path_bck + datetime.now().strftime("%Y%m%d-%H%M%S") + '_' + fic_csv )

       # API Openweather de collecte de températures Fréequence = 1h00
    #api2 = ("http://api.openweathermap.org/data/2.5/onecall/timemachine?lat=45.187604404569896&lon=5.704903862324499&units=metric&lang=fr&dt=1618992000&APPID=053c09565de3a966efa5b8d39b2969d9")
api2 = "http://api.openweathermap.org/data/2.5/onecall/timemachine?"
params = {
       'lat': '45.187604404569896',
       'lon': '5.704903862324499',
       'type': 'hour',
       'units': 'metric',
       'dt':  '1619913600',
       'lang': 'fr',
       'appid': '053c09565de3a966efa5b8d39b2969d9'
}
response = re.request("GET", api2, params=params)
    #return response
print(response.json())





