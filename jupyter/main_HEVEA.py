import requests as re
import pandas as pd
from pandas.io.json import json_normalize
import json
import csv
import time
import datetime
from datetime import date
from datetime import datetime
import os
import os.path
import shutil

fic_csv = "temp_ext_HEVEA.csv"
fic_json = "temp_ext_HEVEA.json"
fic_api = "API_Openweather.ipynb"

src_path = "~/git/dashboard-temperatures-etic/code/"  # repertoire de collecte du template Jupyter Notebook
data_path = "~/dashboard_etic/temp_ext/"     # repertoire de collecte des températures extérieur
bck_path = "~/dashboard_etic/bck_temp_ext/"  # repertoire de backup des températures extérieur
full_path = os.path.expanduser(data_path)    # création des repertoires de base utilisateur ~ symbole tilde
full_path_bck = os.path.expanduser(bck_path) # création des repertoires de base utilisateur ~ symbole tilde
full_path_src =  os.path.expanduser(src_path) # création des repertoires de base utilisateur ~ symbole tilde
list_dir = [full_path, full_path_bck, src_path] # liste des repertoires d'installation
isfile = os.path.isfile(data_path)           # test présence fichier 


d = datetime.now()
unixtime = time.mktime(d.timetuple())



def create_path():    # création des repertoires de travail
    for i in list_dir:
        if not os.path.exists(i):
            os.makedirs(i)

def backup_data():    # sauvegarde CSV 
    for i in os.listdir((full_path)):
        print(i, "est un fichier")
        shutil.copyfile(full_path + fic_csv, full_path_bck + datetime.now().strftime("%Y%m%d-%H%M%S") + '_' + fic_csv )

def remove_file():    # suppression du fichier JSON de l'API
    if os.path.exists(full_path + fic_json):
        os.remove(full_path + fic_json)

def cp_src_file():    # copie du template Jupyter dans data_path
    shutil.copyfile(full_path_src + fic_api, full_path + fic_api)

def get_api():        # API Openweather de collecte de températures Fréequence = 1h00
    
    api2 = "http://api.openweathermap.org/data/2.5/onecall/timemachine?"
    params = {
       'lat': '45.76114750773147',
       'lon': '4.8334077976416205',
       'type': 'hour',
       'units': 'metric',
       #'dt':  '1620518400',
        'dt':  str(int(unixtime)),
       'lang': 'fr',
       'appid': '053c09565de3a966efa5b8d39b2969d9'
    }
    
    response = re.request("GET", api2, params=params)
    return response

def conversion(test):   #conversion des dates Unix Timestamp --> human date
    
    data_dict = test.json()
    with open(full_path + fic_json, "a") as write_file:
        json.dump(data_dict, write_file)
    
    df1 = pd.json_normalize(data_dict['hourly'])
    df1.dt = pd.to_datetime(df1['dt'],unit='s')
    selected_columns = df1[["dt","temp"]]
    df2 = selected_columns.copy()
    df2 = df2.set_index('dt')

    df3 = df2.resample('30min').interpolate().round(2)  # rééchantillonnage et interpolation 

    with open(full_path + fic_csv, "a") as f:
        df3.to_csv(f, header=f.tell()==0, index = True) #La méthode tell() de l’objet fichier retourne la position actuelle du curseur. Par conséquent, si le fichier est vide ou n’existe pas, f.tell==0 est True, de sorte que header est mis à True ; sinon, header est mis à False.
        
    

create_path()
remove_file()
cp_src_file()
var = get_api()
conversion(var)
backup_data()