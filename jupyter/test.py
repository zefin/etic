import pandas as pd
import datetime
from datetime import date
import csv

d = datetime.date.today()
print(d)

col_list = ["dt", "temp"]
df = pd.read_csv("temp_ext.csv", usecols=col_list)

df1 = df.shape
print(df1)

df.info()

df2 = df.loc[0]
print(df2)

def count_line():
    with open("temp_ext.csv", 'r') as f:
        reader = csv.reader(f, delimiter=';')
        for row in reader:
            print(row)
        print(reader.line_num)

count_line()