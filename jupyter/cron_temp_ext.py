from crontab import CronTab

cron = crontab("FBO")
job = cron.new(command='python3 /home/FBO/git/etic/jupyter/main_COOP.py')
job.day.every(1)

cron.write()