def create_csv():
    entetes = [
        u'Time',
        u'101 UCPA',
        u'111 UCPA',
        u'213 PP',
        u'Temp EXT'
    ]

#valeurs = [
#     [u'Valeur1', u'Valeur2', u'Valeur3', u'Valeur4', u'Valeur5'],
#     [u'Valeur6', u'Valeur7', u'Valeur8', u'Valeur9', u'Valeur10'],
#     [u'Valeur11', u'Valeur12', u'Valeur13', u'Valeur14', u'Valeur15']
#]

    f = open('temperatures_etic_hevea.csv', 'w')
    ligneEntete = ";".join(entetes) + "\n"
    f.write(ligneEntete)
#for valeur in valeurs:
#     ligne = ";".join(valeur) + "\n"
#     f.write(ligne)
    f.close()

def import_csv():
    import csv
 
    with open("2005-1.csv", "r") as fsrce:
        with open("temperatures_etic_hevea.csv", "w", newline='') as fdest:
         my_reader = csv.reader(fsrce, delimiter = ';')
         my_writer = csv.writer(fdest, delimiter = ';')
         for ligne in my_reader: # ligne est une liste de valeurs de colonnes
                my_writer.writerow([ligne[0]]) # ligne[0] est la valeur de la 1ère colonne de la ligne considéré


create_csv()
import_csv()     